package com.example.externalapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Film {

    private String title;
    private String original_title;
    private String original_title_romanised;
    private String description;
    private String director;
    private String producer;
    private String release_date;
    private String image;
}
